# Kubernetes Volume Management

En un clúster Kubernetes, els contàiners dels Pods poden tant generar dades com consumir-les. Mentres que les dades del propi contàiner s'espera que no surtin d'ell, altres dades necessiten estar fora per agregar-les i possiblement carregades per les eines d'anàlisi. Kubernetes fa servir `Volums` de molts tipus i altres formes d'emmagatzematge per a l'administració de les dades del contàiner. Els objectes que ens ajuden amb els volums són `PersistentVolume` i `PersistentVolumeClaim`.

Com ja sabem, els contàiners executant-se als Pods són de naturalesa efímera, el que vol dir que tota les dades guardades dins del contàiner s'eliminen si aquest finalitza per algún crash. Tot i que *kubelet* el reiniciarà, ho farà de forma neta, el que vol dir que no tindrà cap dada anterior.

Per a solucionar aquest problema, Kubernetes fa servir els **volums**. Un volum és bàsicament un directori extern als contàiners. Aquest directori, el contingut i l'accés venen determinats per el `Volume Type (tipus de volum)`.

```bash
    --------------------                   --------------------
    |    Contàiner     |                   |     Contàiner    |
    --------------------                   --------------------
                       |                   |
                       |                   |
                       |                   |
                      -----------------------
                      |       Volum         |
                      -----------------------
```

A Kubernetes, el volum es vincula a un Pod i es pot compartir amb tots els contàiners del Pod. Això permet que les dades es preservin encara que el contàiner es reinicii.

## Volum Types (tipus de volums)

El directori que s'ha montat dins del Pod es veu respaldat a baix nivell pel `Volum Type`.  El *Volum Type* decideix les propietats del directori, com la mida, el contingut, els modes d'accés per defecte... Alguns exemples són:

* `emptyDir`: Aquest volum es crea per al Pod en el mateix moment que aquest arrenca en el node treballador. La vida del volum va lligada al Pod, pel que si el Pod finalitza, el contingut d'aquest s'esborra per sempre.

* `hostPath`: Amb aquest volum, podem compartir un directori del host on ens trobem. Si el Pod finalitza, el contingut segueix estant disponible al host.

* `gcePersistentDisk`:  Amb aquest tipus de volum, podem montar un volum **Google Compute Engine (GCE) persistent disk** a un Pod.

* `awsElasticBlockStore`: Amb aquest, podem montar un `AWS EBS Volume` a un Pod.

* `azureDisk`: Podem montar un `Microsoft Azure Data Disk` a un Pod.

* `azureFile`: Podem montar un `Microsoft Azure File Volume` a un Pod.

* `cephfs`: Podem montar un volum *CephFS* existent a un Pod. Quan el Pod finalitza, el volum es desmonta i el contingut es preserva.

* `nfs`: Podem montar un volum *NFS* a un Pod.

* `iscsi`: Podem montar un volum *iSCSI* a un Pod.

* `secret`: Amb aquest tipus, podem passar informació sensible, tal com contrasenyes, als Pods.

* `configMap`: Amb els objectes *configMap* podem proveïr dades de configuració, o comandes shell i arguments al Pod.

* `persistentVolumeClaim`: Podem vincular un *PersistentVolume* a un Pod fent servir *persistentVolumeClaim*.

## PersistentVolumes

En un entorn típic, l'emmagatzematge l'administraven els administradors i els usuaris només rebien les instruccions de com accedir a les dades.

Amb el contàiners ens trobem amb algo similar però és un repte més gran degut a tots els tipus de volums que hem vist. Kubernetes resol aquest problema amb el subsistema dels **volums persistents (PV)**, el qual proveeix APIs per als usuaris i administradors per administrar i consumir les dades persistents. Per a administrar-ho fa servir el tipus de recurs API `PersistentVolume`, i per a consumir-ho, fa servir el `PersistentVolumeClaim`.

Un volum persistent és un emmagatzematge connectat per xarxa al clúster, el qual és provist per l'administrador.

Els volums persistents es poden proporcionar de forma dinàmica basat en el recurs `StorageClass`. L'*StorageClass* conté paràmetres i proveïdors pre-definits  per crear un volum persistent. Fent servir `PersistentVolumeClaims` l'usuari envia una petició per la creació d'un PV dinàmic, el qual es transfereix al recurs *StorageClass*.

Alguns dels tipus de volums que suporta l'administració del emmagatzematge fent servir volums persistents són:

* GCEPersistentDisk

* AWSElasticBlockStore

* AzureFile

* AzureDisk

* CephFS

* NFS

* iSCI

Un exemple de volum persistent amb el tipus **hostPath**:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    # On ho montarem dintre del Pod

    - mountPath: /test-pd
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # localització al host

      path: /data
      # amb directory el directori HA d'existir i tindre els permisos
      # necessaris
      # si volem que el crei en cas de que no hi sigui hem de ficar
      # DirectoryOrCreate (el crearà amb permisos 0755)
      # També pot ser File/OrCreate, Socket, CharDevice, BlockDevice

      type: Directory
```

## PersistentVolumeClaims

**PersistentVolumeClaims (PVC)** és una petició d'emmagatzematge feta per l'usuari. L'usuari demana per un recurs de volum persistent basat en el tipus, mode d'accés i mida. 

Hi han tres tipus de mode d'accés:

* `ReadWriteOnce`: Read-write per un sol node.

* `ReadOnlyMany`: Read-only per varis nodes.

* `ReadWriteMany`: Read-write per varis nodes.

Un cop es troba un volum persistent adeqüat, es vincula al *PersistentVolumeClaim*. Un cop s'ha vinculat correctament, es pot fer servir en un Pod.

Quan l'usuari finalitza el seu treball, el volum persistent es pot **alliberar**. Es pot **reclamar** (per un administrador per a verificar i/o afegir dades), **esborrar** (tant les dades com el volum s'esborren) o **reciclar per un ús futur** (només s'esborren les dades).

Un exemple de configuració de PVC (el nom del PVC ha de ser un nom de subdomini DNS vàlid):

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
```

I, per cridar-lo des d'un Pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```

## Container Storage Interface (CSI)

Sovint els orquestrador com Kubernetes, Mesos, Docker o Cloud Foundry solien tindre els seu mètodes propis per administrar l'emmagatzematge extern fent servir volums. Els venedors d'emmagatzematge i les comunitat de diferents orquestradors van començar a treballar junts per a estandaritzar l'interfície dels volums; un plugin per volums construit amb l'estandard CSI dissenyat per treballar a diferents orquestradors.

Actualment amb CSI, els proveïdors d'emmagatzematge poden desenvolupar solucions sense la necessitat de tocar el codi base de Kubernetes.
