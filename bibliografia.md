* [What is Kubernetes? - Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)

* [Learn Kubernetes Basics - Kubernetes](https://kubernetes.io/docs/tutorials/kubernetes-basics/)

* [Curs Introduction to Kubernetes](https://www.edx.org/course/introduction-to-kubernetes)

* [Documentació Minikube](https://minikube.sigs.k8s.io/docs/start/)

* [Documentació Minikube instal·lació amb Docker](https://minikube.sigs.k8s.io/docs/drivers/docker/)

* [Install and Set Up kubectl - Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)

* [Documentació Kubernetes instal·lació Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)

* [Documentació ordre kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)

* [Web IU Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)

* [Controlador d'admissions habilitats per defecte](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#which-plugins-are-enabled-by-default)

* [Documentació volums a Kubernetes (oficial)](https://kubernetes.io/docs/concepts/storage/volumes/)

* [Documentació volums persistents (oficial)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

* [Documentació de PersistentVolumeClaims (oficial)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)
