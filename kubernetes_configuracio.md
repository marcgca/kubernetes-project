# Configuració e instal·lació de Kubernetes

Podem instal·lar Kubernetes fent servir diferents configuracions. Els quatre tipus d'instal·lacions són:

* `All-in-One Single-Node Installation`: En aquesta configuració, tots els components tant del node mestre com del treballador són instal·lats i executats en un únic node. És útil per aprendre, desenvolupament i testing, però no s'ha de fer servir a producció. Un exemple és **Minikube**.

* `Single-Node etcd, Single-Master and Multi-Worker Installation`: En aquesta configuració tenim un únic node mestre, que també executa una única instància d'etcd. Múltiples nodes treballadors es connecten al node mestre.

* `Single-Node etcd, Multi-Master and Multi-Worker Installation`: En aquesta configuració tenim varis nodes mestre configurats en mode HA (*High Availability*), però sol un node amb una instància d'etcd. Múltiples nodes treballadors es connecten al nodes mestre.

* `Multi-Node etcd, Multi-Master and Multi-Worker Installation`: En aquesta configuració tenim etcd configurat en un clúster en mode HA, els nodes mestre també estàn configurats en mode HA i els nodes treballadors estàn connectats a aquests. Aquesta és la configuració més avançada i recomenada per a configuracions de producció.

## Eines/Recursos útils

Tenim varies eines/recursos útils per a l'instal·lació o administració de clústers:

* `kubeadm`: És una forma segura i recomenada per a l'arrencada tant de clústers en mode single o multi node. Té un conjunt de blocs per a configurar el clúster, però és molt senzill d'extendre afegint-li més característiques.
  
  Kubeadm no suporta l'aprovisionament de hosts.

* `kubespray`: Amb *kubespray* (també conegut com *kargo*) podem instal·lar clústers HA (*Highly Available*) a ***AWS***, ***GCE***, ***Azure***, ***OpenStack*** o ***localment***.
  
  Està basat en *Ansible* i està disponible a la majoria de distribucions de Linux.

* `kops`: Amb *kops* podem crear, destruir, actualitzar, i mantenir el grau de producció, clústers HA (*Highly Available*) des de la línia de comandes. També pot subministrar les màquines.
  
  Actualment, ***AWS*** és oficialment suportat, mentres que el suport per a ***CGE*** es troba en beta. ***VMware vSphere*** es troba en una versió alpha i altres plataformes estàn planejades per al futur.

* `kube-aws`: Amb *kube-aws* podem crear, actualitzar i destruir clústers a ***AWS*** des de la línia de comandes.

## Minikube

*Minikube* és la forma més senzilla i recomanada per a arrencar clústers *all in one* de forma local a les nostres màquines. S'instal·la directament en un Linux, macOS o Windows local. Tot i això, i per a aprofitar totes les aventatges que ofereix *Minikube*, s'hauria d'instal·lar un **type-2 Hypervisor**, per a executar-se en conjunt amb Minikube. El que fa Minikube amb l'hipervisor és invocar-lo i crear una VM on executarà el clúster single node. 

Requeriments per a executar *Minikube*:

* `kubectl`: *kubectl* és un binari que accedeix i administra qualsevol clúster de Kubernetes. És necessaria la seva instal·lació per a poguer administrar el clúster.

* `Type-2 Hypervisor`: Per a Linux poden ser **VirtualBox** o **KVM**. Hem d'assegurar-nos de que tenim l'opció de virtualització habilitada a la nostra BIOS (*VT-x* o *AMD-v* depenent de la nostra CPU)
  
  * Tot i això, *Minikube* suporta l'opció `--vm-driver=none` que executa el components de Kubernetes directament al sistema local i no a una VM. Per a aquesta opció és necessari tindre instal·lat **Docker**, per lo que no necessitem l'instal·lació d'un hipervisor. Si fem servir aquesta opció ens hem d'assegurar d'especificar una *bridge network* (?) per a Docker, si no pot canviar entre reinicis de xarxa, cuasant la pèrdua de connectivitat al clúster.

* Conexió a internet la primera execució de *Minikubes* per a la descàrrega de paquets, dependèndices, actualitzacions i descàrrega d'imatges necessàries.
