# Tasques a realitzar

* Actualitzar traefik del 1.7 al 2(mirar quina versió va millor si 2.2 o 2.3)
  
  * [Traefik v1 to v2 - Traefik](https://doc.traefik.io/traefik/v2.2/migration/v1-to-v2/) (veure si es fa servir el IngressRoute de Traefik o l'Ingress de Kubernetes)
  
  * [Kubernetes and Let's Encrypt - Traefik](https://doc.traefik.io/traefik/v2.2/user-guides/crd-acme/)

* Mirar si es pot fer el downgrade de kubernetes a la versió 1.16 (actualment 1.18) per a fer el upgrade de la 1.14 a la 1.16
  
  * No es pot fer un downgrade del clúster si ja el tenim iniciat, però podem optar per una d'aquests opcions:
    
    * Esborrar el clúster i crear-lo de nou amb la versió de kubernetes que volem:
      
      ```bash
      1) Recreate the cluster with Kubernetes v1.16.15, by running:
      
          minikube delete
          minikube start --kubernetes-version=1.16.15
      ```
    
    * Crear un altre clúster amb la versió dessitjada:
      
      ```bash
      2) Create a second cluster with Kubernetes v1.16.15, by running:
      
          minikube start -p minikube2 --kubernetes-version=1.16.15
      
      # Significat de la -p
      -p, --profile string                   The name of the minikube VM being used. This can be set to allow having multiple instances of minikube independently. (default "minikube")
      ```
  
  * [Deprecated APIs Removed In 1.16: Here’s What You Need To Know | Kubernetes](https://kubernetes.io/blog/2019/07/18/api-deprecations-in-1-16/)

* Montar localment un moodle (amb server web, base de dades, dades persistents...)

* Un cop montat, implementar l'estructura en un clúster kubernetes (investigar configmap, tema volums persistents...)

* Investigar si hi ha alguna eina de traducció en viu per streamings (o amb delay)
