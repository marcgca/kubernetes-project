# Instal·lació de traefik a kubernetes

Guia per l'ús de traefik v1.7 a Kubernetes [Kubernetes - Traefik](https://doc.traefik.io/traefik/v1.7/user-guide/kubernetes/)

Seguirem la guia mencionada amb alguns canvis segons la versió de kubernetes que tinguem, en aquest cas tenim la **1.19.2**. Es diràn els canvis que es fan comparant-los amb l'anterior.

## Prerequisits

Per a instal·lar Traefik a Kubernetes hem de tindre en compte aquests punts:

* Un clúster de Kubernetes funcionant.

* Activar l'addon **ingress**:
  
  ```bash
  # A Minikube el podem activar amb la següent ordre
  minikube addons enable ingress
  ```

* Tindre **kubectl** instal·lat.

## Role Based Access Control

A partir de Kubernetes 1.6+ s'introdueix el *RBAC* que permet un control dels recursos de Kubernetes i de l'API. Si el nostre clúster està configurat amb RBAC, necessitarem autoritzar Traefik per a que pugui fer servir l'API de Kubernetes.

Hi han dos formes de fer-ho:

* Via namespaces específics amb **RoleBindings**

* Via global amb **ClusterRoleBinding**

En aquest farem servir la segona opció.

* En el camp `apiVersion` ens indica que la del exemple està deprecated i quina hem d'utilitzar (fitxer traefik-rbac.yaml):
  
  ```yaml
  # apiVersion: rbac.authorization.k8s.io/v1beta1
  apiVersion: rbac.authorization.k8s.io/v1
  ---
  kind: ClusterRole
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    name: traefik-ingress-controller
  rules:
    - apiGroups:
        - ""
      resources:
        - services
        - endpoints
        - secrets
      verbs:
        - get
        - list
        - watch
    - apiGroups:
        - extensions
      resources:
        - ingresses
      verbs:
        - get
        - list
        - watch
    - apiGroups:
      - extensions
      resources:
      - ingresses/status
      verbs:
      - update
  ---
  kind: ClusterRoleBinding
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    name: traefik-ingress-controller
  roleRef:
    apiGroup: rbac.authorization.k8s.io
    kind: ClusterRole
    name: traefik-ingress-controller
  subjects:
  - kind: ServiceAccount
    name: traefik-ingress-controller
    namespace: kube-system
  ```

## Llençar Traefik amb Deployment o DaemonSet

En aquest cas farem servir **DaemonSet** amb l'exemple que ens donen.

* Després d'executar-ho haurem de comprovar que se'ns ha llençat el pod corresponent. Si tenim error perque un altre controlador està fent servir el port escollit podem seleccionar un altre port lliure o treure el controlador si no el volem (scale=0).

## Ingress

En aquest pas crearem un *Service* i l'*Ingress* corresponent amb alguns canvis (ja que la versió que utilitza de l'api està deprecated)

Hem de canviar varies coses per a l'apartat de l'ingress:

```yaml
# La versió de la api
# apiVersion: extensions/v1beta1 #
apiVersion: networking.k8s.io/v1
# Els següents camps
# spec:
#  rules:
#  - host: traefik-ui.minikube
#    http:
#      paths:
#      - path: /
#        backend:
#          serviceName: traefik-web-ui
#          servicePort: web
spec:
  rules:
  - host: traefik-ui.minikube
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: traefik-web-ui
            port:
              number: 8080
```

Amb això se'ns queda aixi:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: traefik-web-ui
  namespace: kube-system
spec:
  selector:
    k8s-app: traefik-ingress-lb
  ports:
  - name: web
    port: 80
    targetPort: 8080
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: traefik-web-ui
  namespace: kube-system
spec:
  rules:
  - host: traefik-ui.minikube
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: traefik-web-ui
            port:
              number: 8080
```

Quan volguem crear un nou recurs Ingress per a que ho rebi el traefik ho hem d'indicar amb la anotació al camp `kubernetes.io/ingress.class` tal com veurem a continuació:

## Possibles errors i solucions

* Error: El pod no s'inicia per un error de ports lliures:
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl --namespace=kube-system get pods
  NAME                                        READY   STATUS      RESTARTS   AGE
  coredns-66bff467f8-6gllc                    1/1     Running     12         150d
  coredns-66bff467f8-jbtvj                    1/1     Running     12         150d
  etcd-minikube                               1/1     Running     9          148d
  ingress-nginx-admission-create-j64pj        0/1     Completed   0          80m
  ingress-nginx-admission-patch-pvlwt         0/1     Completed   0          80m
  ingress-nginx-controller-7bb4c67d67-vkdm9   1/1     Running     1          80m
  kube-apiserver-minikube                     1/1     Running     9          148d
  kube-controller-manager-minikube            1/1     Running     10         150d
  kube-proxy-2f9ts                            1/1     Running     10         150d
  kube-scheduler-minikube                     1/1     Running     10         150d
  storage-provisioner                         1/1     Running     19         150d
  traefik-ingress-controller-z7lt9            0/1     Pending     0          11m
  ```

* Bàsicament no el pot engegar perque ja hi ha un controlador (ingress-nginx-controller-7bb4c67d67-vkdm9) al port que vol el controlador del traefik.

* Podem canviar els ports o eliminar aquest controlador escalant els pods a 0.
