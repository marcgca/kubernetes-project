# Estructura instal·lació Moodle en local

Instal·larem Moodle en local amb 2 contàiners, un servidor **nginx** i una base de dades **postgres**.

Farem servir una xarxa per a que es puguin comunicar entre ells amb el hostname.

En els dos casos farem servir volums per a que les dades siguin persistents.

Seguirem la docu de moodle per a l'instal·lació https://docs.moodle.org/39/en/Installing_Moodle#Set_up_your_server

## Nginx

Farem servir l'imatge oficial de nginx de [Docker Hub](https://hub.docker.com/_/nginx) i el volum **moodle** que hem creat abans. 

* Moodle fa servir php, pel que hem de baixar els paquets necessaris per a l'instal·lació.
* Hem de tindre el servei executant-se, pel que quan iniciem el contàiner li hem d'indicar que s'executi en foreground.

```bash
# Si no el tenim:
docker volume create moodledata
docker volume create moodle-db
```

Descarreguem l'instal·lador de Moodle i copiem el directori al directori del volum:

```bash
[marc@localhost moodle]$ sudo cp -r ~/Downloads/moodle-latest-39 /var/lib/docker/volumes/moodle/_data/
```

Ara, quan montem el volum al contàiner tindrem l'instal·lador de moodle.

```bash
docker run  --name=nginxmoodle --network moodle -h nginxmoodle  -v moodledata:/usr/share/nginx/moodledata -d marcgc/nginx-moodle
```

Hem de tindre en compte que després de fer l'instal·lació hem de guardar el contàiner, si no es perdràn les dades.

```bash
docker exec -it nginxmoodle /bin/bash
```

### Conf php

Per a tindre el mòdul de php executant-se i configurat hem de tindre en compte els següents punts:

* Tindre el mòdul instal·lat:
  
  * A Debian es php-fpm
    
    ```bash
    apt-get install -y php-fpm
    ```

* Un cop instal·lat, hem de configurar que el propi nginx arrenqui el socket que necessita:
  
  * A Debian trobem que l'usuari que té per defecte és `www-data`. El canviem per el de `nginx`:
    
    ```bash
    sed -i 's/listen.owner = www-data/listen.owner = nginx/g' /etc/php/7.3/fpm/pool.d/www.conf
    sed -i 's/listen.group = www-data/listen.group = nginx/g' /etc/php/7.0/fpm/pool.d/www.conf
    ```

* Arrenquem el servei:
  
  ```bash
  root@nginxmoodle:/# php-fpm7.3 -F
  [27-Oct-2020 09:04:28] NOTICE: fpm is running, pid 39
  [27-Oct-2020 09:04:28] NOTICE: ready to handle connections
  [27-Oct-2020 09:04:28] NOTICE: systemd monitor interval set to 10000ms
  ```
  
  * Si ens trobem que no el pot executar perque el directori `php` a `run` no existeix, el creem i ja serà capaç de crear el socket.

### Conf nginx

Per a que nginx executi el socket de php primer hem de configurar-lo.

Crearem un fitxer de configuració anomenat `nginxmoodle.conf` a `/etc/nginx/conf.d/` amb el següent contingut:

```bash
server {
    listen         80 default_server;
    listen         [::]:80 default_server;
    server_name    _;
    root           /usr/share/nginx/html/;
    index          index.php index.html index.htm;

  location ~ [^/]\.php(/|$) {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_index index.php;
    fastcgi_pass unix:/run/php/php7.3-fpm.sock;
    include         fastcgi_params;
    fastcgi_param   PATH_INFO          $fastcgi_path_info;
    fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
    fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
  }

}
```

Després d'això hem de reiniciar el servidor nginx amb `nginx -s reload`.

### Instal·lació moodle

Per a instal·lar Moodle ho podem fer via web o via CLI.

En el nostre cas ho farem via web accedint a on es troba dintre del nostre servidor.

* Directori web: `nginxmoodle/moodle`

* Directori on es troba al servidor: `/usr/share/nginx/html/moodle`

* Directori de moodledata (montat amb volum): `/usr/share/nginx/moodledata`

* Direcció base de dades: `postgresmoodle`

* Nom base de dades: `moodle`

* Usuari base de dades: `moodleuser`

* Pass user: `userm00dle`

* Pass admin a moodle: `m00dle` 

## Postgres

Farem servir l'imatge oficial de postgres de [Docker Hub](https://hub.docker.com/_/postgres) i el volum **moodle-db** que hem creat.

Arrencarem postgres:

```bash
$ docker run --name postgresmoodle --network moodle -e POSTGRES_PASSWORD=mysecretpassword -v moodle-db:/var/lib/postgresql/data -h postgresmoodle -d marcgc/postgres-moodle
```

Ens hi connectem per accedir a la base de dades:

```bash
$ docker exec -it postgresmoodle /bin/bash
```

Accedim a la base de dades especificant l'usuari postgres:

```bash
root@postgresmoodle:/# su - postgres
postgres@postgresmoodle:~$ psql
psql (13.0 (Debian 13.0-1.pgdg100+1))
Type "help" for help.
```

* Si tenim el volum creat i ja hem creat l'usuari i la base de dades no hem de fer el següent pas
  
  * Moodle ens demana una nova base de dades i un usuari, pel que les crearem seguin la seva docu [PostgreSQL - MoodleDocs](https://docs.moodle.org/39/en/PostgreSQL):

```plsql
# Creació usuari moodle
postgres=# CREATE USER moodleuser WITH PASSWORD 'userm00dle';
CREATE ROLE
# Creació base de dades
postgres=# CREATE DATABASE moodle WITH OWNER moodleuser;
CREATE DATABASE
# Ens assegurem que s'ha creat amb les especificacions que li hem indicat
postgres=# \l
                               List of databases
   Name    |   Owner    | Encoding | Collate |  Ctype  |   Access privileges   
-----------+------------+----------+---------+---------+-----------------------
 docker    | docker     | UTF8     | C.UTF-8 | C.UTF-8 | 
 moodle    | moodleuser | UTF8     | C.UTF-8 | C.UTF-8 | 
 postgres  | postgres   | UTF8     | C.UTF-8 | C.UTF-8 | 
 template0 | postgres   | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |            |          |         |         | postgres=CTc/postgres
 template1 | postgres   | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |            |          |         |         | postgres=CTc/postgres
(5 rows)
```

## Executar-ho en un clúster

Un cop tinguem les imatges creades, les passarem a un clúster de kubernetes.

En aquest cas farem servir el clúster creat per minikube amb volums externs per les dades, tal com haviem fet amb el proves de contàiners.

* `IMPORTANT`: Hem de tindre en compte que Minikube és el node mestre, pel que si volem muntar un volum del host ha d'estar-hi. Per això **primer** hem de montar el directori al Minikube.
  
  ```bash
  minikube mount <source directory>:<target directory>
  
  # Aquesta ordre es queda enganchada a la terminal on s'executa
  [marc@localhost ~]$ minikube mount /home/marc/Documents/repos/kubernetes-project/practiques_moodle/moodle/nginx/moodledata:/home/marc/Documents/repos/kubernetes-project/practiques_moodle/moodle/nginx/moodledata -p minikube2
  ```

* Crearem un configmap amb la configuració de moodle
  
  ```bash
  [marc@localhost nginx]$ kubectl create configmap moodleconfig --from-file=config.php
  configmap/moodleconfig created
  ```

* Per a executar un contàiner específic d'un pod que en té varis executarem la següent ordre
  
  ```bash
  kubectl exec -ti nompod -c nomcontainer /bin/bash
  #
  kubectl exec -ti moodle-deployment-7974bb56fb-lrrz6 -c postgresmoodle /bin/bash
  ```

minikube unknown error 526 al intentar llegir un directori de postgresmoodle
