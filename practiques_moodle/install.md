# Instal·lació del programari necessari

Necessitarem instal·lar *docker* i *minikube* a un Ubuntu 18.04

## Instal·lació de docker

Seguirem l'instal·lació segons ens proporciona la seva [documentació](https://docs.docker.com/engine/install/ubuntu/)

* Primer ens assegurem de no tindre versions anteriors (en cas de no tindre'n ens ho dirà):
  
  ```bash
  $ sudo apt-get remove docker docker-engine docker.io containerd runc
  ```

* Instal·larem fent servir el repositori de Docker.
  
  * Primer hem d'instal·lar els paquets que ens requereix per a que `apt` fagi servir un repositori amb HTTPS:
    
    ```bash
    $ sudo apt-get update
    
    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    ```
  
  * Afegim la clau GPG oficial de Docker:
    
    ```bash
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    ```
  
  * Comprovem que tenim la correcte:
    
    ```bash
    $ sudo apt-key fingerprint 0EBFCD88
    
    pub   rsa4096 2017-02-22 [SCEA]
          9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
    uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
    sub   rsa4096 2017-02-22 [S]
    ```
  
  * Ara afegirem el repositori stable:
    
    ```bash
    $ sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    ```
  
  * Actualitzem i instal·lem la última versió:
    
    ```bash
     $ sudo apt-get update
     $ sudo apt-get install docker-ce docker-ce-cli containerd.io
    ```
  
  * Executem el contàiner *hello-world* per comprovar que s'ha instal·lat correctament:
    
    ```bash
    $ sudo docker run hello-world
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    0e03bdcc26d7: Pull complete 
    Digest: sha256:4cf9c47f86df71d48364001ede3a4fcd85ae80ce02ebad74156906caff5378bc
    Status: Downloaded newer image for hello-world:latest
    
    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    
    [...]
    ```

## Instal·lació de Minikube

Primer hem d'instal·lar el packet de *kubectl* des de la seva documentació [Install and Set Up kubectl | Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

* Seguim les ordres de l'apartat d'Ubuntu:
  
  ```bash
  sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
  sudo apt-get update
  sudo apt-get install -y kubectl
  ```

Seguirem l'instal·lació segons la seva [documentació](https://minikube.sigs.k8s.io/docs/start/) a l'apartat *Debian package*

* Executem les següents ordres:
  
  ```bash
  curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
  sudo dpkg -i minikube_latest_amd64.deb
  ```

Un cop instal·lat el podem executar per veure que funciona bé.

* Si no troba cap software de virtualització agafarà per defecte el driver de *Docker*, tot i que el podem ficar per defecte:
  
  ```bash
  minikube config set driver docker
  ```
  
  * Si no, sempre ho podem executar amb `minkube start --driver=docker`

* Com ho executarem sent un usuari *non-root* hem d'afegir l'usuari al grup de docker per a tindre els permisos per a l'execució de minikube. 
  
  * Si no ho fem, no tindrà permisos per accedir o crear el socket necessari i ens retornarà un error. 
  
  * Tampoc podem executar-ho amb *sudo* perque ens retornarà un error que ens impideix executar el driver docker amb privilegis de root. 
  
  ```bash
  # Creem el grup de docker (si ja està creat ens ho avisarà)
  $ sudo groupadd docker
  # Afegim l'usuari al grup
  $ sudo usermod -aG docker $USER
  
  # Activem els canvis
  $ newgrp docker 
  ```

* Un cop fet tot això, podrem executar minikube amb el nostre usuari fent servir *Docker*
  
  ```bash
  intern@intern-OptiPlex-3070:~$ minikube start
  😄  minikube v1.14.0 on Ubuntu 18.04
  ✨  Using the docker driver based on user configuration
  👍  Starting control plane node minikube in cluster minikube
  🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
  🐳  Preparing Kubernetes v1.19.2 on Docker 19.03.8 ...
  🔎  Verifying Kubernetes components...
  🌟  Enabled addons: storage-provisioner, default-storageclass
  🏄  Done! kubectl is now configured to use "minikube" by default
  ```


