# Creació d'un servei i exposició del clúster al exterior

Seguint les explicacions de [Connecting Applications with Services | Kubernetes](https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/)

Tenint per exemple un deployment d'un servidor web nginx, en teoria ens hi podem conectar als pods, però què passa si un node mor? Que els pods moren amb ell. Això farà que el *deployment* en creei de nous, però amb diferents ips, per a això tenim els *Services*.

* Exemple del deployment:
  
  ```yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: webserver
    labels:
      app: nginx
  spec:
    replicas: 3
    selector:
      matchLabels:
        app: nginx
    template:
      metadata:
        labels:
          app: nginx
      spec:
        containers:
        - name: nginx
          image: nginx:alpine
          ports:
          - containerPort: 80
  ```

Un *Service* defineix un set lògic de Pods que proveeixen la mateixa funcionalitat. Quan es crea, se li assigna una IP única (també anomenada *clusterIP*). Aquesta adreça no canvia mentres el *Service* sigui viu. 

El podem crear o bé amb la següent ordre:

```bash
kubectl expose deployment/webserver
```

O creant-lo amb un fitxer *YAML*:

```yaml
# kubectl apply -f webserver-svc.yaml
apiVersion: v1
kind: Service
metadata:
  name: web-service
  labels:
    run: web-service
spec:
  type: NodePort
  ports:
  - port: 80
    protocol: TCP
  selector:
    app: nginx
```

* Si no se l'hi indica el tipus, per defecte el deixa com *ClusterIP*. En aquest cas l'hi indiquem que sigui *NodePort*.

* Diferències entre *ClusterIP* i *NodePort*: 
  
  * **ClusterIP** assigna una IP interna del clúster per a balançejar la càrrega cap als endpoints. Aquests es determinent pel selector o, si no s'ha especifcat, un objecte *Endpoint* creat de forma manual.
  
  * **NodePort** construieix sobre un *ClusterIP* i assigna un port a cada node que enruta cap a la ip del clúster.

Aquest servei que hem creat targeteja el port 80 de qualsevol Pod amb l'etiqueta **app: nginx** i els exposa (veurem a quin port a continuació) :

```bash
$ kubectl get svc web-service
NAME          TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
web-service   NodePort   10.109.44.128   <none>        80:32136/TCP   111m
```

Com podem comprovar, mapeja el port **80** amb el **32136** (és el que farem servir quan hi volguem accedir).

* Hem de tindre en compte que si hem creat el servei **després** del deployment haurem de matar-los i deixar que el *Deployment* els crei de nou.
  
  * Si creem el Pod abans que el servei, no tindrà les variables d'entorn respectives al servei que afegeix *kubelet*:
    
    ```bash
    intern@intern-OptiPlex-3070:~$ kubectl exec webserver-7fb7fd49b4-72q8p -- printenv | grep SERVICE
    KUBERNETES_SERVICE_HOST=10.96.0.1
    KUBERNETES_SERVICE_PORT=443
    KUBERNETES_SERVICE_PORT_HTTPS=443
    # Els matem i els tornem a aixecar
    kubectl scale deployment webserver --replicas=0; \ 
    kubectl scale deployment webserver --replicas=3
    ```
  
  * Les variables d'entorn un cop s'han tornat a crear o si haviem creat el servei abans:
    
    ```bash
    intern@intern-OptiPlex-3070:$ kubectl exec webserver-7fb7fd49b4-55dr4 -- printenv | grep SERVICE
    WEB_SERVICE_PORT_80_TCP_PROTO=tcp
    WEB_SERVICE_PORT_80_TCP_ADDR=10.109.44.128
    KUBERNETES_SERVICE_PORT_HTTPS=443
    WEB_SERVICE_SERVICE_PORT=80
    WEB_SERVICE_PORT_80_TCP=tcp://10.109.44.128:80
    KUBERNETES_SERVICE_PORT=443
    WEB_SERVICE_PORT=tcp://10.109.44.128:80
    WEB_SERVICE_PORT_80_TCP_PORT=80
    KUBERNETES_SERVICE_HOST=10.96.0.1
    WEB_SERVICE_SERVICE_HOST=10.109.44.128
    
    ```

Ara només hem d'accedir a la ip del clúster (`minikube ip` ens valdrà si fem servir Minikube o `kubectl cluster-info`) amb el port que ens ha indicat abans i accedirem a la pàgina de benvinguda de nginx.


