# Projecte d'introducció a Kubernetes

![](../aux/kubernetes-logo.png)

           Autor: Marc Gómez Cardona 

           Curs: 2019/2020

           EDT ASIX

# Què és un contàiner?

* Què és i per a què serveix

![](../aux/Moby-logo.png)

# Què és un Container Orchestrator?

* Què és

* Què ens aporta

* Val la pena?

![](../aux/container_orchestration_tools.jpg)

# Què és Kubernetes?

* Què és?

* D'on prové?

# Característiques de Kubernetes

Kubernetes ofereix un gran número de característiques per a l'orquestració de contàiners, algunes d'aquestes són:

* Self-healing 

* Balanceig de càrrega

* Monitorització

* Rollouts i rollbacks

* ...

# Arquitectura de Kubernetes

* Un o més **master** nodes.

* Un o més **worker** nodes.
  
  ![Estructura de Kubernetes del curs d'EDX](../aux/Kubernetes_Architecture.png)

                                                                                                        *imatge del curs d'edx* 

# Pods

![](../aux/pod.png)

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.15.11
    ports:
    - containerPort: 80
```

# ReplicaSets

```bash
                           replicas: 3
                           template: pod
                          image: nginx:1.9.1
                              /        \
                  antic      /          \      nou                    
                            /            \
   ReplicaSet A            /              \             ReplicaSet B
   ----------------------------            ----------------------------
   |    replicas: 3           |            |    replicas: 3           |
   |    template: pod         |            |    template: pod         |
   |    image: nginx:1.7.9    |            |    image: nginx:1.9.1    |
   |            |             |            |            |             |
   |          / | \           |            |          / | \           |
   |         /  |  \          |            |         /  |  \          |
   |        /   |   \         |            |        /   |   \         |
   |    Pod1  Pod2  Pod3      |            |    Pod1  Pod2  Pod3      |
   ----------------------------            ----------------------------

                    Deployment (ReplicaSet B Created)
```

# Service

```bash
               select app=frontend                  app:frontend
              / service = frontend-svc ------------ 10.0.1.3
             /  VIP = 172.17.0.4                                    
            /                     \               
user/client                        \---------------  app:frontend
            \                                          10.0.1.4  
             \                                   
              \select app=db                    
               service = db-svc ------------------ app:db
               VIP = 172.17.0.5                    10.0.1.10        
```

Els *Services* poden exposar **Pods**, **ReplicaSets**, **Deployments**, **DaemonSets** i **StatefulSets**.

# Exemple de Service

```yaml
kind: Service
apiVersion: v1
metadata:
  name: frontend-svc
spec:
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 5000
```

# ServiceType: ClusterIP i NodePort

```yaml
apiVersion: v1
kind: Service
metadata:
  name: web-service
  labels:
    run: web-service
spec:
  type: NodePort
  ports:
  - port: 80
    protocol: TCP
  selector:
    app: nginx
```

# Liveness i Readiness Probes

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: liveness
  name: liveness-exec
spec:
  containers:
  - name: liveness
    image: k8s.gcr.io/busybox
    args:
    - /bin/sh
    - -c
    - touch /tmp/healthy; sleep 30; rm -rf /tmp/healthy; sleep 600
    livenessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5
      periodSeconds: 5
```

# Kubernetes Volume Management

```bash
    --------------------                   --------------------
    |    Contàiner     |                   |     Contàiner    |
    --------------------                   --------------------
                       |                   |
                       |                   |
                       |                   |
                      -----------------------
                      |       Volum         |
                      -----------------------
```

# Volum Types (tipus de volums)

Alguns exemples són:

* emptyDir

* hostPath

* gcePersistentDisk

* awsElasticBlockStore

* nfs

* secret

* ...

# PersistentVolumes

Un exemple de volum persistent amb el tipus **hostPath**:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /test-pd
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      path: /data
      type: Directory
```

# PersistentVolumeClaims

Hi han tres tipus de mode d'accés:

* ReadWriteOnce

* ReadOnlyMany

* ReadWriteMany

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
```

# Ingress

* TLS (Capa de Transport Segura)

* Hosting virtual basat en el nom

* Balançejador de càrrega

* Regles customitzades

# Conclusions

* Què m'ha semblat Kubernetes

* Opinió personal del projecte

# Fi

Gràcies!
