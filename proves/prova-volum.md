En aquest pràctica crearem un volum local compartit entre dos Pods, hi escriurem alguna cosa, i comprovarem com, després d'eliminar aquests Pods, les dades segueixen estant (és a dir, són persistents)

## Creació volum local

Primer de tot, entrarem a Minikube mitjançant ssh i, un cop dins, crearem un directori.

```bash
[marc@localhost ~]$ minikube ssh
docker@minikube:~$ mkdir pod-volume
docker@minikube:~$ ll
total 44
drwxr-xr-x. 1 docker docker 4096 Jun 17 21:41 ./
drwxr-xr-x. 1 root   root   4096 Apr 27 17:43 ../
-rw-r--r--. 1 docker docker  220 Apr 27 17:43 .bash_logout
-rw-r--r--. 1 docker docker 3771 Apr 27 17:43 .bashrc
-rw-r--r--. 1 docker docker  807 Apr 27 17:43 .profile
drwxr-xr-x. 1 docker docker 4096 May 16 16:49 .ssh/
-rw-r--r--. 1 docker docker    0 May 16 16:49 .sudo_as_admin_successful
drwxr-xr-x. 2 docker docker 4096 Jun 17 21:41 pod-volume/
# Hem de sapiguer a on apuntaràn els nostres Pods
docker@minikube:~/pod-volume$ pwd
/home/docker/pod-volume
```

## Creació dels Pods que el faràn servir

Per a fer servir aquest volum ho hem d'indicar en el fitxer de configuració dels Pods que anomenarem `share-pod.yaml`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: share-pod
  labels:
    app: share-pod
spec:
  volumes:
  - name: host-volume
    hostPath:
      path: /home/docker/pod-volume
  containers:
  - image: debian
    name: debian
    volumeMounts:
    - mountPath: /host-vol
      name: host-volume
    command: ["/bin/sh", "-c", "echo aquest es el debian > /host-vol/dades.txt; sleep 3600"]
  - image: fedora
    name: fedora
    volumeMounts:
    - mountPath: /host-vol
      name: host-volume
    command: ["/bin/sh", "-c", "echo aquest es el fedora >> /host-vol/dades.txt; sleep 3600"]
```

Com podem observar, el que estem creant són dos contàiners dins d'un Pod (un debian i un fedora)  que montan el volum i escriuen en un fitxer.

Creem el Pod:

```bash
[marc@localhost config_files]$ kubectl create -f share-pod.yaml 
pod/share-pod created
# Comprovem que el Pod s'hagi creat i estigui corrent
[marc@localhost config_files]$ kubectl get pods
NAME                           READY   STATUS              RESTARTS   AGE
share-pod                      0/2     ContainerCreating   0          9s
[marc@localhost config_files]$ kubectl get pods
NAME                           READY   STATUS    RESTARTS   AGE
share-pod                      2/2     Running   0          19s
```

## Comprovació dades locals

Ara mirarem si, s'ha creat el fitxer i cada contàiner ha executat la seva ordre, desant així el contingut en el local (del node mestre en aquest cas per ser Minikube)

```bash
# Hem de tindre en compte que estem a /home/docker/pod-volume
docker@minikube:~/pod-volume$ ll
total 20
drwxr-xr-x. 2 docker docker 4096 Jun 17 22:27 ./
drwxr-xr-x. 1 docker docker 4096 Jun 17 21:41 ../
-rw-r--r--. 1 root   root     40 Jun 17 22:47 dades.txt
docker@minikube:~/pod-volume$ cat dades.txt 
aquest es el debian
aquest es el fedora
```

Com podem comprovar, s'ha creat correctament

## Persistencia de les dades: esborrar el Pod

Com ja sabem, els Pods son de naturalesa efímera (i per tant, les seves dades també) ara comprovarem com, esborrant el Pod, les dades segueixen estant en local.

```bash
[marc@localhost ~]$ kubectl delete pod share-pod
pod "share-pod" deleted
[marc@localhost ~]$ kubectl get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-basic-6898fd4dcf-6lm92   1/1     Running   2          137m
nginx-basic-6898fd4dcf-qxjkt   1/1     Running   2          136m
nginx-basic-6898fd4dcf-xmkqq   1/1     Running   2          136m
webserver-97499b967-5s6ws      1/1     Running   3          30h
webserver-97499b967-5v448      1/1     Running   3          30h
webserver-97499b967-x7x52      1/1     Running   3          30h
```

Tornem a comprovar que el fitxer està en local i que, evidentment, les dades també:

```bash
docker@minikube:~/pod-volume$ ll
total 20
drwxr-xr-x. 2 docker docker 4096 Jun 17 22:27 ./
drwxr-xr-x. 1 docker docker 4096 Jun 17 21:41 ../
-rw-r--r--. 1 root   root     40 Jun 17 22:47 dades.txt
docker@minikube:~/pod-volume$ cat dades.txt 
aquest es el debian
aquest es el fedora
```


