A aquest prova desplegarem un nginx bàsic amb 2 pods, l'escalarem a 3 pods, actualitzarem la versió del nginx i tornarem a la versió anterior

## Desplegament

Despleguem amb la interfície web un deployment anomenat **nginx-basic** amb 2 Pods amb l'imatge **nginx:1.15-alpine**

O podem crear-ho amb un fitxer de configuració com aquest, anomenat `nginx_basic.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-basic
  labels:
    app: nginx-basic
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-basic
  template:
    metadata:
      labels:
        app: nginx-basic
    spec:
      containers:
      - name: nginx
        image: nginx:1.15-alpine
        ports:
        - containerPort: 80
```

## Escalat a 3 pods

```bash
[marc@localhost kubernetes-project]$ kubectl get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-basic-66d8b85c8f-27pj6   1/1     Running   0          2m4s
nginx-basic-66d8b85c8f-q6r5l   1/1     Running   0          2m4s
#
[marc@localhost kubernetes-project]$ kubectl scale deploy nginx-basic --replicas=3
deployment.apps/nginx-basic scaled
#
[marc@localhost kubernetes-project]$ kubectl get deploy,rs,po -l k8s-app=nginx-basic
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-basic   3/3     3            3           4m48s

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-basic-66d8b85c8f   3         3         3       4m48s

NAME                               READY   STATUS    RESTARTS   AGE
pod/nginx-basic-66d8b85c8f-27pj6   1/1     Running   0          4m48s
pod/nginx-basic-66d8b85c8f-q6r5l   1/1     Running   0          4m48s
pod/nginx-basic-66d8b85c8f-rjwr5   1/1     Running   0          88s
```

## Actualització de la versió i comprovació estats

Primer comprovem que sol tenim l'estat inicial, associat a l'imatge 1.15

```bash
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic# E
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
1         <none>
#
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=1
deployment.apps/nginx-basic with revision #1
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=66d8b85c8f
  Containers:
   nginx-basic:
    Image:    nginx:1.15-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
# Actualitzem la imatge
[marc@localhost kubernetes-project]$ kubectl set image deployment nginx-basic nginx-basic=nginx:1.16-alpine
deployment.apps/nginx-basic image updated
# Si mirem els estats ens trobem amb 2
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
# Si veiem l'informació del nou estat, veiem que té la nova imatge
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=2
deployment.apps/nginx-basic with revision #2
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=85c8bdb7c8
  Containers:
   nginx-basic:
    Image:    nginx:1.16-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
# Si mirem l'informació del deployment ens trobem que té 2 replicasets, un per cada estat
# I que aquest té 0 Pods, mentres que el nou té els 3
[marc@localhost kubernetes-project]$ kubectl get deploy,rs,po -l k8s-app=nginx-basic
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-basic   3/3     3            3           23m

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-basic-66d8b85c8f   0         0         0       23m
replicaset.apps/nginx-basic-85c8bdb7c8   3         3         3       5m7s

NAME                               READY   STATUS    RESTARTS   AGE
pod/nginx-basic-85c8bdb7c8-bx2q9   1/1     Running   0          3m35s
pod/nginx-basic-85c8bdb7c8-clnzj   1/1     Running   0          3m34s
pod/nginx-basic-85c8bdb7c8-nrv5d   1/1     Running   0          5m7s
```

## Rollback a un estat anterior

Si volguèssim, pel que fos, tornar a una versió anterior, hem de sapiguer a quina i quin és el seu estat

```bash
[marc@localhost kubernetes-project]$ kubectl rollout undo deployment nginx-basic --to-revision=1
deployment.apps/nginx-basic rolled back
# I si mirem el history ens trobem amb això
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
2         <none>
3         <none>
# L'estat 1 ara és la 3, sent l'estat actual
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=2
deployment.apps/nginx-basic with revision #2
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=85c8bdb7c8
  Containers:
   nginx-basic:
    Image:    nginx:1.16-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=5
deployment.apps/nginx-basic with revision #5
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=66d8b85c8f
  Containers:
   nginx-basic:
    Image:    nginx:1.15-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
# I com podem veure, el replicaset més antic és qui té els Pods actius
[marc@localhost kubernetes-project]$ kubectl get deploy,rs,po -l k8s-app=nginx-basic
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-basic   3/3     3            3           37m

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-basic-66d8b85c8f   3         3         3       37m
replicaset.apps/nginx-basic-85c8bdb7c8   0         0         0       18m

NAME                               READY   STATUS    RESTARTS   AGE
pod/nginx-basic-66d8b85c8f-k7st7   1/1     Running   0          4m8s
pod/nginx-basic-66d8b85c8f-kp8n2   1/1     Running   0          4m9s
pod/nginx-basic-66d8b85c8f-z4s52   1/1     Running   0          4m7s
```

## Quants estats desa?

Podem fer la prova afegint una nova versió de l'imatge:

```bash
[marc@localhost kubernetes-project]$ kubectl set image deployment nginx-basic nginx-basic=nginx:1.17-alpine
deployment.apps/nginx-basic image updated
# I comprovem que tenim un nou estat associat a la nova versió
[marc@localhost kubernetes-project]$ kubectl rollout history deployment nginx-basic
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
4         <none>
5         <none>
6         <none>
#
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=6
deployment.apps/nginx-basic with revision #6
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=6898fd4dcf
  Containers:
   nginx-basic:
    Image:    nginx:1.17-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
```

Per tant per cada nova versió de l'imatge afegeix un nou estat, però si tornem a un estat **anterior**, tot i que canvia el número, segueixen havent-hi els **mateixos** estats.
