# Ordres/fitxers de kubernetes

## kubectl

Amb *kubectl* controlem el clúster, és el client CLI de Kubernetes:

* `cluster-info`: obtenim informació del clúster
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl cluster-info
  Kubernetes master is running at https://172.17.0.2:8443
  KubeDNS is running at https://172.17.0.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
  
  To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
  ```

* `get deployments`: obtenim informació dels deployments
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl get deployments
  NAME        READY   UP-TO-DATE   AVAILABLE   AGE
  webserver   3/3     3            3           6m42s
  ```

* `get replicasets`: obtenim informació dels replicasets
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl get replicasets
  NAME                   DESIRED   CURRENT   READY   AGE
  webserver-5d58b6b749   3         3         3       6m55s
  ```

* `get pods`:  obtenim informació dels pods
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl get pods
  NAME                         READY   STATUS    RESTARTS   AGE
  webserver-5d58b6b749-gsl2d   1/1     Running   0          8m28s
  webserver-5d58b6b749-k7r8g   1/1     Running   0          8m28s
  webserver-5d58b6b749-z4d42   1/1     Running   0          8m28s
  # Amb l'opció -l obtenim els pods d'una etiqueta en particular
  [marc@localhost kubernetes-project]$ kubectl get pods -l k8s-app=webserver
  NAME                         READY   STATUS    RESTARTS   AGE
  webserver-5d58b6b749-gsl2d   1/1     Running   0          6h3m
  webserver-5d58b6b749-k7r8g   1/1     Running   0          6h3m
  webserver-5d58b6b749-z4d42   1/1     Running   0          6h3m
  ```

* `describe pod nomPod`: obtenim informació del Pod especificat:
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl describe pod webserver-5d58b6b749-z4d42
  Name:         webserver-5d58b6b749-z4d42
  Namespace:    default
  Priority:     0
  Node:         minikube/172.17.0.2
  Start Time:   Tue, 16 Jun 2020 12:01:27 +0200
  Labels:       k8s-app=webserver
                pod-template-hash=5d58b6b749
  Annotations:  <none>
  Status:       Running
  IP:           172.18.0.8
  IPs:
    IP:           172.18.0.8
  Controlled By:  ReplicaSet/webserver-5d58b6b749
  Containers:
    webserver:
      Container ID:   docker://34c4a37b61c6b3866a676779c63766b18b942cb33e637ed23809bc7dfbb5daad
      Image:          nginx:alpine
      Image ID:       docker-pullable://nginx@sha256:b89a6ccbda39576ad23fd079978c967cecc6b170db6e7ff8a769bf2259a71912
      Port:           <none>
      Host Port:      <none>
      State:          Running
        Started:      Tue, 16 Jun 2020 12:02:49 +0200
      Ready:          True
      Restart Count:  0
      Environment:    <none>
      Mounts:
        /var/run/secrets/kubernetes.io/serviceaccount from default-token-llmhz (ro)
  Conditions:
    Type              Status
    Initialized       True 
    Ready             True 
    ContainersReady   True 
    PodScheduled      True 
  Volumes:
    default-token-llmhz:
      Type:        Secret (a volume populated by a Secret)
      SecretName:  default-token-llmhz
      Optional:    false
  QoS Class:       BestEffort
  Node-Selectors:  <none>
  Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                   node.kubernetes.io/unreachable:NoExecute for 300s
  Events:          <none>
  ```
  
  

* `delete deployments nom`: eliminem el desployment especificat. Hem de tindre en compte que si esborrem el *Deployment* també esborrarem els *ReplicaSets* i els *Pods* corresponents.
  
  ```bash
  [marc@localhost kubernetes-project]$ kubectl delete deployments webserver
  deployment.apps "webserver" deleted
  [marc@localhost kubernetes-project]$ kubectl get replicasets
  No resources found in default namespace.
  [marc@localhost kubernetes-project]$ kubectl get pods
  No resources found in default namespace. 
  ```

* `create`: creem un nou *Deployment*. Amb l'opció *-f*  indiquem el fitxer que farem servir:
  
  ```bash
  [marc@localhost config_files]$ kubectl create -f webserver.yaml 
  deployment.apps/webserver created
  ```

## Exemple fitxer d'un deployment

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webserver
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
```

## Exemple fitxer d'un Pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.15.11
    ports:
    - containerPort: 80
```
