# Instal·lació Minikube

Descarreguem el paquet i l'instal·lem com ens indica a la seva [docu](https://minikube.sigs.k8s.io/docs/start/):

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
sudo rpm -ivh minikube-latest.x86_64.rpm
```

Per a arrencar-lo juntament amb *Docker* ho indiquem amb el *start* tal com ens indica la seva [docu](https://minikube.sigs.k8s.io/docs/drivers/docker/):

```bash
# Executem el clúster fent server Docker
minikube start --driver=docker
# Si volguèssim executar Docker com a default
minikube config set driver docker
```

Just després d'executar-lo ens demanarà que instal·lem `kubectl`:

```bash
[marc@localhost ~]$ minikube start
😄  minikube v1.10.1 en Fedora 29
✨  Using the docker driver based on existing profile
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.18.2 preload ...
    > preloaded-images-k8s-v3-v1.18.2-docker-overlay2-amd64.tar.lz4: 525.43 MiB
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparando Kubernetes v1.18.2 en Docker 19.03.2...
    ▪ kubeadm.pod-network-cidr=10.244.0.0/16
🔎  Verifying Kubernetes components...
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"
💡  Para disfrutar de un funcionamiento óptimo, instala kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/
```

L'instal·lem tal com ens indica a la docu [Install and Set Up kubectl - Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)

```bash
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubectl
```

Un cop instal·lat i arrencat, podem comprobar el seu estat amb `status` tal com ens indica la seva [docu](https://kubernetes.io/docs/tasks/tools/install-minikube/):

```bash
# Mirem l'estat
[marc@localhost ~]$ minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
# Per a parar el clúster
minikube stop
```

# Accés al clúster

Podem accedir al clúster (ens val per a tots els clúster de kubernetes) de diferents formes:

* Mitjançant eines i scripts de CLI (*Command Line Interface*)

* Interfícies d'usuari mitjançant un navegador web

* Api's

## CLI

`kubectl` és el client CLI de Kubernetes per a administrar els recursos del clúster i les aplicacions. El podem fer servir tant com a ordre en la terminal o com a part de scripts d'eines d'automització. El podem fer servir remotament si els punts d'accesos i les credencials estàn configurades.

## Interfície web

`Kubernetes Dashboard` proveeix una interfície d'usuari basada en web per a interactuar amb un clúster de Kubernetes per a administar aquest i les aplicacions containeritzades.

Podem accedir-hi directament executant la següent ordre:

```bash
[marc@localhost kubernetes-project]$ minikube dashboard
🔌  Enabling dashboard ...
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:36391/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

També podem accedir-hi si executem primer l'ordre `kubectl proxy`. En aquest cas, si parem el proxy no hi podrem accedir.

![Kubernetes Dashboard](./aux/Kubernetes_dashboard.png)

## APIs

Com ja sabem, Kubernetes té un **API Server**, i ens hi podem connectar des de l'exterior per a interactuar amb el clúster. Podem connectar directament al servidor API amb els seus *endpoints* e introduïr-hi comandes, sempre que tinguem acces al node mestre i els permisos corresponents.

```bash
                                /

/healtz        /metrics        /api        /apis        ....

                  /api/v1                /apis/apps    /apis/...

   /api/v1/pods    /api/v1/nodes        /apis/apps/v1

                               /apis/apps/Deployment    /apis/apps/...
```

Podem tindre els següents espais HTTP:

* `Core Group`: Aquest grup inclou els objectes tals com Pods, Serveis, nodes, noms d'espai...

* `Named Group`: Aquest grup inclou els objectes en format /apis/$NAME/version... Aquestes versions poden incloure diferents nivells d'estabilitat (alpha, beta, stable)

* `System-wide`: Aquest grup consisteix en els *endpoints* de l'API, tal com */healthz*, */logs*, */metrics*...

Amb `kubectl proxy` executant-se, podem enviar requests al API server mitjançant *localhost* al port corresponent:

```bash
# Rebem els endpoints que té el server
curl http://localhost:8001/
{
 "paths": [
   "/api",
   "/api/v1",
   "/apis",
   "/apis/apps",
   ......
   ......
   "/logs",
   "/metrics",
   "/openapi/v2",
   "/version"
 ]
}
```

* En el cas de que no s'estigui executant `kubectl proxy` ens tindrem que autenticar directament al servidor API quan enviem les request.
  
  Podem fer-ho a través d'un *Bearer Token*, que és un token d'accés generat pel servidor autenticador (el servidor API al node mestre)  en el qual ens podem connectar de nou.
  
  ```bash
  # Conseguim el token
  TOKEN=$(kubectl describe secret -n kube-system $(kubectl get secrets -n kube-system | grep default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t' | tr -d " ")
  # Conseguim l'endpoint
  APISERVER=$(kubectl config view | grep https | cut -f 2- -d ":" | tr -d " ")
  # Comprovem que tenim la mateixa ip mirant les següents ordres
  echo $APISERVER
  kubectl cluster-info
  # Finalment accedim
  curl $APISERVER --header "Authorization: Bearer $TOKEN" --insecure
  # També podem accedir-hi sense el token d'accés, extraient el certificat del client, la clau del client i el certificat d'autoritat del fitxer .kube/config
  # Un cop s'extreuen, són encodejats i passats amb la comanda curl per l'autenticació
  ```

# Creació d'un deployment

## Des del dashboard

Per a crear un nou deployment des del dashboard hem de seguir els següents pasos (tenim en compte de que tenim Minikube engegat i executant-se):

* Seleccionar el botó `+` de la part superior dreta.
  
  ![create new deployment 1](./aux/crea1.png)

* Ens apareixerà una interfície per crear-ho inserint el contingut d'un fitxer *YAML* o *JSON* , exportant el propi fitxer o creant-ho al propi dashboard. En aquest cas escollirem la tercera opció.
  
  ![crear deployment 2](./aux/crear2.png)

* Aquí podem escollir el nom que volguem, la imatge en la que es basaràn, el número de Pods... A *Show advanced options* podem especificar les etiquetes, el nom d'espai, les variables d'entorn... Per defecte, l'etiqueta *app* amb el valor *webserver* (el mateix nom que li donem) se li fica al nom de l'app.
  
  ![crear nou deployment dashboard](./aux/crea3.png)

* Un cop li donem al botó `Deploy` executem tota la creació. El `Deployment` **webserver** crearà el `ReplicaSet` **webserver-5d58b6b749** i aquest crearà els `Pods` especificats **webserver-5d58b6b749-X** (sent X el número del Pod). Tindrem una interfície on podrem veure tota la informació de cada element i el seu estat.
  
  ![crear nou deployment amb dashboard 4](./aux/crear4.png)

Podem comprovar que s'ha creat correctament si mirem amb `kubectl`:

```bash
[marc@localhost kubernetes-project]$ kubectl get deployments
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
webserver   3/3     3            3           6m42s
[marc@localhost kubernetes-project]$ kubectl get replicasets
NAME                   DESIRED   CURRENT   READY   AGE
webserver-5d58b6b749   3         3         3       6m55s
[marc@localhost kubernetes-project]$ kubectl get pods
NAME                         READY   STATUS    RESTARTS   AGE
webserver-5d58b6b749-gsl2d   1/1     Running   0          8m28s
webserver-5d58b6b749-k7r8g   1/1     Running   0          8m28s
webserver-5d58b6b749-z4d42   1/1     Running   0          8m28s
```

El podem eliminar amb l'opció `delete deployments nomDep`. Hem de tindre en compte de que si esborrem el *Deployment* també esborrarem els *ReplicaSets* i *Pods* que s'hagin creat per ell:

```bash
[marc@localhost kubernetes-project]$ kubectl delete deployments webserver
deployment.apps "webserver" deleted
[marc@localhost kubernetes-project]$ kubectl get replicasets
No resources found in default namespace.
[marc@localhost kubernetes-project]$ kubectl get pods
No resources found in default namespace.
```

## Des de CLI

Podem crear un nou deployment des de la línia de comandes a través d'un fitxer. En aquest exemple el crearem amb un fitxer *YAML*

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webserver
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
```

Per a fer-ho utilitzem la comanda `kubectl` junt amb l'opció `create` i l'argument `-f` per indicar-li el fitxer de configuració:

```bash
[marc@localhost config_files]$ kubectl create -f webserver.yaml 
deployment.apps/webserver created
```

Com podem comprovar, el *Deployment* ha creat el *Replicaset* i aquest els *Pods*:

```bash
[marc@localhost config_files]$ kubetl get deployments
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
webserver   3/3     3            3           3m7s
[marc@localhost config_files]$ kubectl get replicasets
NAME                  DESIRED   CURRENT   READY   AGE
webserver-97499b967   3         3         3       3m13s
[marc@localhost config_files]$ kubectl get pods
NAME                        READY   STATUS    RESTARTS   AGE
webserver-97499b967-5s6ws   1/1     Running   0          3m17s
webserver-97499b967-5v448   1/1     Running   0          3m17s
webserver-97499b967-x7x52   1/1     Running   0          3m17s
```

Si ens connectem a la IP del clúster amb el port mapejat, ens connectarà amb l'nginx

```bash
[marc@localhost config_files]$ minikube ip
172.17.0.2
```

![nginx del clúster](./aux/nginx_cluster.png)
